package movieLens

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}


object TopRatingsApp extends App{

   val conf=new SparkConf().setAppName("TopRatings")
      .setMaster("local[*]")
   val sc = new SparkContext(conf)

   val ratings: RDD[(Int, Double)]  =
      sc.textFile("/Users/mac027/Downloads/drive-download-20170515T120401Z-001/ratings.csv")
        //逗號分割
        .map(_.split(","))
        .filter(_(0)!="userId")
        .map(strs=>{
          //printIn(strs.mkString(","))
           (
             strs(1).toInt,    // movieId
             strs(2).toDouble  // rating
           )
        })
  //sum score(false表示取後不放回):
    val totalRatingByMovieId=
    ratings.reduceByKey((acc,curr)=>acc+curr)
  totalRatingByMovieId.takeSample(false,5).foreach(println)
  // average score:
    val averageRatingByMovieId:RDD[(Int,Double)]={
      ratings.mapValues(v=> v->1)}
    .reduceByKey((acc,curr)=>{
      (acc._1+curr._1)->(acc._2+curr._2)
    })
      .mapValues(kv=> kv._1/kv._2.toDouble)

  // 前10排名(false：從小到大排)：
  val top10=averageRatingByMovieId.sortBy(_._2,false).take(10)
  top10.foreach(println)

  val movies=
    sc.textFile("/Users/mac027/Downloads/drive-download-20170515T120401Z-001/movies.csv")
        .map(_.split(","))
      .filter(_(0)!="movieId")
      .filter(_(1)!="title")
      .filter(_(2)!="genres")
    .map(strs=>{
      (strs(0).toInt,strs(1))
    })
     movies.take(10).foreach(println)

  val joined:RDD[(Int,(Double,String))]=averageRatingByMovieId.join(movies)
  joined.takeSample(false,5).foreach(println)

  joined.saveAsTextFile("result")
  joined.map(v=>v._1+","+v._2._2+v._2._2+","+v._2._1).saveAsTextFile("result")



    //ratings.take(5).foreach(println)

}

