import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac005 on 2017/5/1.
  */
object SquareApp extends App {

  val conf=new SparkConf().setAppName("Square").setMaster("local[*]")

  val sc=new SparkContext(conf)

  val intRdd=sc.parallelize(1 to 10000)

  val squares=intRdd.map(x=>x*x)

  squares.take(10).foreach(println)

}
